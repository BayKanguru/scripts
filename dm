#!/bin/sh

# dmenu script that uses both path and xdg desktop files

CACHE_DIR="$XDG_CACHE_HOME"/dm
CACHE="$CACHE_DIR"/cache

CLR="$(tput setaf 4)"
NOCLR="$(tput sgr0)"

process_desktop() {
  file="$(cat $1)"
  name="$(echo "$file" | sed -ne "s/^Name=//p" | sed -n 1p)"
  exec="$(echo "$file" | sed -ne "s/^Exec=//p" | sed -n 1p)"
  echo "$name	$exec"
}

process_bin() {
  echo "$(basename $1)	$1"
}

display() {
  echo "$CLR >> $1$NOCLR"
}

refresh_cache() {
  # create/reset the cache
  if [[ ! -d "$CACHE_DIR" ]]
  then
    mkdir "$CACHE_DIR"
  fi
  if [[ -f "$CACHE" ]]
  then
    rm "$CACHE"
  fi
  touch $CACHE

  display "Looking through XDG Desktop files"
  dirs="$(echo "$XDG_DATA_DIRS" | sed "s/:/\/applications /g")/applications $XDG_DATA_HOME/applications"
  echo -e "$(echo $dirs | tr " " "\n")"
  for file in $(find $dirs -iname "*.desktop")
  do
    process_desktop "$file" >> $CACHE
  done

  display 'Looking through $PATH'
  echo -e "$(echo $PATH | tr ":" "\n")"
  for file in $(dmenu_path)
  do
    process_bin "$file" >> "$CACHE"
  done

  display "Sorting the cache"
  sort -f "$CACHE" -o "$CACHE"
}

run() {
  # take first column of cache and feed it to dmenu
  name="$(sed --regexp-extended -n "s/^(.*)\t(.*)/\1/p" "$CACHE" | dmenu -i $@ | sed 's/^[[:space:]]*//;s/[[:space:]]*$//')"
  # find corresponding command in cache and remove freedesktop stuff
  cmd="$(awk -v name="$name" -F "\t" '{gsub(/^ */, ""); gsub(/ *$/, ""); gsub(/%[fuFUick]/, ""); if ($1==name) {print $2}}' "$CACHE")"

  /bin/sh -c "$cmd" &
}

if [[ "$DM_REFRESH" ]]
then
  refresh_cache
else
  run $@
fi
